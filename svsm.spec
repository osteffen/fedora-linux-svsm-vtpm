Name: svsm-vtpm
Version: 0.1
Release:        1%{?dist}
Summary: Secure VM Service Module for AMD SEV-SNP with vTPM

License: MIT
URL: https://github.com/svsm-vtpm/linux-svsm
Source0: linux-svsm.tar.xz

Patch0001: 0001-Bump-submodule-versions-for-libm-libcrt.patch
Patch0002: 0002-Add-rust-toolchain-file.patch
Patch0003: 0003-build.rs-set-include-path.patch
Patch0004: 0004-build.rs-Debug-print-library-build-commands.patch
Patch0005: 0005-build.rs-Improve-error-handling-of-external-commands.patch
Patch0006: 0006-Pin-Rust-to-1.69.0-nightly.patch

BuildRequires: git
BuildRequires: pkg-config
BuildRequires: autoconf
BuildRequires: autoconf-archive
BuildRequires: automake
BuildRequires: gcc
BuildRequires: libtool
BuildRequires: clang
BuildRequires: curl
BuildRequires: make
BuildRequires: diffutils

%description
Secure Virtual machine Service Module for use in
confidenial VMs based on AMD SEV-SNP memory encryption.
This service module includes a virtual TPM device.


%global debug_package %{nil}

%prep
%autosetup -S git -v -n linux-svsm -D


%build

# Default flags interfere with svsm build
unset CFLAGS
unset CCFLAGS
unset LDFLAGS

# install Rust
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh /dev/fd/0 \
  -y \
  --target x86_64-unknown-none \
  -c rust-src \
  -c llvm-tools-preview
source "${HOME}/.cargo/env"
cargo install xargo bootimage

# Not sure why this is needed?
rustup component add rust-src

# Pretend we ran `make prereq`
touch .prereq

make

%install
mkdir -p %{buildroot}%{_datadir}/%{name}
install \
  -m 444 \
  svsm.bin \
  %{buildroot}%{_datadir}/%{name}/svsm-vtpm.bin


%files
%dir %{_datadir}/%{name}/
%{_datadir}/%{name}/svsm-vtpm.bin


%changelog
* Mon Feb 6 2023 Oliver Steffen <osteffen@redhat.com> - 0.0.1
- Initial version of the package
